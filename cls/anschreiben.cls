%% anschreiben.cls
%% Copyright 2023 Mathezirkel Augsburg
%
% This work may be distributed and/or modified under the
% conditions of the LaTeX Project Public License, either version 1.3
% of this license or (at your option) any later version.
% The latest version of this license is in
%   http://www.latex-project.org/lppl.txt
% and version 1.3 or later is part of all distributions of LaTeX
% version 2005/12/01 or later.
%
% This work has the LPPL maintenance status `maintained'.
%
% The Current Maintainer of this work is Felix Stärk.
%
% This work consists of all files listed in manifest.txt.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%                     														  %%
%% Klasse für Anschreiben des Mathezirkels                                    %%
%%                                                                            %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{anschreiben}[2021/10/13 LaTeX class]

\LoadClass[
    DIN,
    a4paper,
    ngerman,
    foldmarks=false,   % keine Falzmarker
    fromalign=right,   % Logo links
    fromlogo=true,     % Logo anzeigen
    backaddress=plain, % Backaddress nicht unterstrichen
    enlargefirstpage,
    firstfoot=false,
    ]{scrlttr2}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Pakete                                                                     %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Sprache und Schrift
\RequirePackage[ngerman]{babel}
\RequirePackage[utf8]{inputenc}
\RequirePackage[T1]{fontenc}
\RequirePackage{lmodern}

% Mathematische Pakete
\RequirePackage{amsmath,amsthm,amssymb, amsfonts, mathtools, stmaryrd}

% Pakete für Graphiken
\RequirePackage{xcolor}
\RequirePackage{graphicx}

% Pakete für Links
\RequirePackage{qrcode}
\RequirePackage{hyperref}
\RequirePackage{url}

% Pakete für Serienbriefe
\RequirePackage{datatool}
\DTLsetseparator{;}

% Pakete für Scripting
\RequirePackage{ifthen}

% Pakete für Listen
\RequirePackage{enumitem}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Einstellbare Variablen                                                     %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\newcommand{\nameA}{}
\newcommand{\nameB}{}
\newcommand{\signatur}{}
\newcommand{\mailA}{}
\newcommand{\mailB}{}
\newcommand{\telefon}{}
\newcommand{\betreff}{}
\newcommand{\datum}{}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Layout                                                                     %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Abstände und Position
\setlength{\textwidth}{16cm}

\newplength{lmargin}
\newplength{rmargin}
\newplength{tmargin}
\setplength{lmargin}{2.5cm} % Linker Rand
\setplength{rmargin}{2.5cm} % Rechter Rand
\setplength{tmargin}{2cm} % Oberer Rand

\setplength{firstheadhpos}{\useplength{lmargin}}
\setplength{toaddrhpos}{\useplength{lmargin}}
\setplength{refhpos}{\useplength{lmargin}}
\setplength{firstfoothpos}{\useplength{lmargin}}

\setplength{firstheadvpos}{\useplength{tmargin}}

\setplength{lochpos}{\useplength{rmargin}}
\setplength{locwidth}{0.4\textwidth}
\setplength{locvpos}{4.5cm}

\setplength{subjectaftervskip}{1em}

\renewcommand*{\raggedsignature}{\raggedright}
\setplength{sigbeforevskip}{1em}


%% Briefkopf

% Logo
\setkomavar{fromlogo}{\includegraphics[scale=0.1]{logo-ifm.png}}

% Rücksendeadresse
\setkomavar{backaddress}{\tiny{Uni Augsburg \\ Mathezirkel \\ Inst. f. Math. \\ 86135 Augsburg}}
\setkomavar{backaddressseparator}{-- }

% Datum wird im Location-Block eingefügt
\setkomavar{date}[refline=nodate]{}

% Absender
\setkomavar{location}{%
    \textbf{\nameA}\par
    \ifthenelse{\equal{\nameB}{}}{}{\textbf{\nameB}}\medskip

    Mathezirkel\par
    Universität Augsburg\par
    Institut für Mathematik\par
    86135 Augsburg\medskip

    \telefon\par
    \textsf{\mailA}\par
    \ifthenelse{\equal{\mailB}{}}{}{\textsf{\mailB}}\medskip

    \rule{0.4\textwidth}{0.25mm}\par
    Augsburg, \datum
}

% Betreff
\setkomavar{subject}{\betreff}

% Signatur
\setkomavar{signature}{\signatur}


% Satz
\setlength{\parindent}{0pt} % keine Einrückung in der ersten Zeile eines Absatzes
\pagestyle{empty}           % keine Fußzeile

\clubpenalty=10000
\widowpenalty=10000
\displaywidowpenalty=10000


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Methoden                                                                   %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Erzeuge Links mit QR-Code
\newcommand{\urlQR}[1]{%
    \begin{center}
        \begin{minipage}[t]{0.7\linewidth}
            \url{#1}
        \end{minipage}
        \begin{minipage}[t]{0.2\linewidth}
            \qrcode[height=1.7cm]{#1}
        \end{minipage}
    \end{center}
}

% Automatische Anrede anhand des Geschlechts
\RequirePackage{xstring}
\newcommand{\anrede}[2]{%
    \IfStrEqCase{#1}{%
        {w}{Liebe~#2,}
        {m}{Lieber~#2,}
        {d}{Hey~#2,}
        {x}{Liebe~Schüler:innen,}}
}
