%% zettel2.cls
%% Copyright 2023 Mathezirkel Augsburg
%
% This work may be distributed and/or modified under the
% conditions of the LaTeX Project Public License, either version 1.3
% of this license or (at your option) any later version.
% The latest version of this license is in
%   http://www.latex-project.org/lppl.txt
% and version 1.3 or later is part of all distributions of LaTeX
% version 2005/12/01 or later.
%
% This work has the LPPL maintenance status `maintained'.
%
% The Current Maintainer of this work is Felix Stärk.
%
% This work consists of all files listed in manifest.txt.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%                     														  %%
%% Class for documents of the Mathezirkel                                     %%
%%                                                                            %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Class options                                                              %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{zettel2}[2022/04/03 LaTeX class]

\LoadClass[a4paper,ngerman]{scrartcl}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Packages                                                                   %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Language and fonts
\RequirePackage[ngerman]{babel}
\RequirePackage[utf8]{inputenc}
\RequirePackage[T1]{fontenc}
\RequirePackage{lmodern}

% Mathematics
\RequirePackage{amsmath, amsthm, amssymb, amsfonts, mathtools, stmaryrd}

% Graphics and color
\RequirePackage[table]{xcolor}
\definecolor{mathezirkel}{RGB}{50,32,139}
\RequirePackage{graphicx}

% Links
\RequirePackage{qrcode}
\RequirePackage{hyperref}
\RequirePackage{url}
\RequirePackage{lastpage}

% Enumeration
\RequirePackage{enumitem}

% Tabulars
\RequirePackage{tabularx}
\RequirePackage{longtable}
\RequirePackage{multirow}

% Datetime
\RequirePackage[showdow=false,calc]{datetime2}
% \DTMdisplaystyle prints the date without year
% \DTMDisplaystyle prints the date containing the year
\DTMnewdatestyle{defaultdateformat}{%
    \renewcommand{\DTMdisplaydate}[4]{%
        \ifDTMshowdow\DTMweekdayname{##4},~den~\fi
        \DTMtwodigits{##3}.~\DTMmonthname{##2}%
    }
    \renewcommand{\DTMDisplaydate}[4]{%
        \ifDTMshowdow\DTMweekdayname{##4},~den~\fi
        \DTMtwodigits{##3}.~\DTMmonthname{##2}~\number##1%
    }
}
\DTMsetdatestyle{defaultdateformat}
\DTMnewtimestyle{defaulttimeformat}{%
    \renewcommand{\DTMdisplaytime}[3]{%
        \DTMtwodigits{##1}.\DTMtwodigits{##2}%
    }
}
\DTMsettimestyle{defaulttimeformat}

% Programming
\RequirePackage{ifthen}
\RequirePackage{xstring}

% Datatool
\usepackage{datatool}
\DTLsetseparator{;}

% Layout
\RequirePackage{framed}
\RequirePackage{scrlayer-scrpage}

\RequirePackage{setspace}
\RequirePackage[top=2cm,bottom=2cm,left=2cm,right=2cm,footskip=1cm]{geometry}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Adjustable variables                                                       %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% These variables may be overwritten in the document file.

\definecolor{shadecolor}{rgb}{.97,.97,.97}

\newcommand{\address}{%
    Universität Augsburg \\
    Institut für Mathematik \\
    Mathezirkel \\
    86135 Augsburg}

\renewcommand{\subject}{}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Layout                                                                     %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\setlength{\parskip}{\medskipamount}
\setlength{\parindent}{0pt}

\clubpenalty=10000
\widowpenalty=10000
\displaywidowpenalty=10000

\setlength{\unitlength}{1cm}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Methods                                                                    %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%
% \urlQR{<link>}
%
% Places a QR code besides the argument containing its text.
%
\newcommand{\urlQR}[1]{%
    \begin{center}
        \begin{minipage}[t]{0.7\linewidth}
            \url{#1}
        \end{minipage}
        \begin{minipage}[t]{0.2\linewidth}
            \qrcode[height=1.7cm]{#1}
        \end{minipage}
    \end{center}
}

%
% Letter head
%

% Determine the logo on the left
% You have to use syntax of the picture environment
\newcommand{\leftLogo}{\put(0,-0.8){\includegraphics[scale=0.1]{logo-ifm}}}

% Determine the logo on the right
% You have to use syntax of the picture environment
\newcommand{\rightLogo}{\put(13.3,-4.5){\includegraphics[scale=0.18]{cover}}}

% Prints the specified logos onto the page
\newcommand{\makeheader}{%
    \begin{picture}(0,0)
        \leftLogo
        \rightLogo
    \end{picture}
}

% Prints the address at the desired position
\newcommand{\makeaddress}[1][0.5,-2.5]{%
    \begin{picture}(0,0)
        \put(#1){\vtop{\address}}
    \end{picture}\par
}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Formular fields                                                            %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%
% Formularfelder
%
% \dotline[<gap>=2pt]{<width>}
%
% Defines a dotted line of a given length. The variable <gap> defines the length
% of the gap between the dots. The default value is 2pt's.
%
% \dotlinefill[<gap>=2pt]
%
% Produces a dotted line filling the maximum free space within the line. The
% variable <gap> defines the length of the gap between the dots. The default
% value is 2pt's.
%
\makeatletter
\newcommand{\dotlinefill}[1][2pt]{\leavevmode \cleaders \hb@xt@ #1{\hss .\hss }\hfill \kern \z@}
\makeatother
\newcommand{\dotline}[2][2pt]{\leavevmode\hbox to #2{\dotlinefill[#1]\hfil}}

%
% Checkbox
%
% \checkbox[<checked>=0]
%
% Produces a checkbox. The variable <checked> indicates whether it is checked or not.
%     0: unchecked
%     1: checked
% The default is unchecked.
%
\newcommand{\checkbox}[1][0]{%
    \ifnum#1=1
        \ensuremath{\ \boxtimes\ }
    \else
        \ifnum#1=0
            \ensuremath{\ \square\ }
        \fi
    \fi
}

%
% Themabox
%
% An environment for checkboxes
%
\newenvironment{themabox}[1]{%
        \renewcommand{\labelitemi}{\checkbox}
        \begin{enumerate}[labelindent=0pt,labelwidth=2.35cm,itemindent=0pt,align=left,leftmargin=!]
            \item[\textbf{#1}]
            \begin{itemize}
    }{%
        \end{itemize}
        \end{enumerate}
        \vspace{0.3cm}
}
