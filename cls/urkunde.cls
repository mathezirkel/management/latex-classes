%% urkunde.cls
%% Copyright 2023 Mathezirkel Augsburg
%
% This work may be distributed and/or modified under the
% conditions of the LaTeX Project Public License, either version 1.3
% of this license or (at your option) any later version.
% The latest version of this license is in
%   http://www.latex-project.org/lppl.txt
% and version 1.3 or later is part of all distributions of LaTeX
% version 2005/12/01 or later.
%
% This work has the LPPL maintenance status `maintained'.
%
% The Current Maintainer of this work is Felix Stärk.
%
% This work consists of all files listed in manifest.txt.

\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{urkunde}[2022/07/15 LaTeX class]

\LoadClass[a4paper,ngerman]{scrartcl}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Packages                                                                   %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Language and fonts
\RequirePackage[ngerman]{babel}
\RequirePackage[utf8]{inputenc}
\RequirePackage[T1]{fontenc}
\RequirePackage{lmodern}

% Mathematics
\RequirePackage{amsmath, amsthm, amssymb, amsfonts, mathtools, stmaryrd}

% Graphics and color
\RequirePackage{xcolor}
\definecolor{mathezirkel}{RGB}{50,32,139}
\RequirePackage{graphicx}

% Links
\RequirePackage{hyperref}
\RequirePackage{url}

% Enumeration
\RequirePackage{enumitem}

% Programming
\RequirePackage{ifthen}
\RequirePackage{xstring}

% Datatool
\RequirePackage{datatool}
\DTLsetseparator{;}

\RequirePackage{setspace}
\RequirePackage{geometry}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Layout                                                                     %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\geometry{tmargin=1cm,bmargin=1.5cm,lmargin=2cm,rmargin=2cm}

\setlength{\parskip}{\medskipamount}
\setlength{\parindent}{0pt}

\clubpenalty=10000
\widowpenalty=10000
\displaywidowpenalty=10000

\setlength{\unitlength}{1cm}

\pagestyle{empty}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Methods                                                                    %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\newcommand{\leiter}[4]{
    \ifthenelse{\equal{#4}{}}{
        \ifthenelse{\equal{#3}{}}{
            \ifthenelse{\equal{#2}{}}{
                #1
            }{
                #1, #2
            }
        }{
            #1, #2, #3
        }
    }{
        \parbox[t]{10cm}{\hspace*{\fill} #1, #2, \\ \hspace*{\fill} #3, #4}
    }
}

\newcommand{\urkunde}[9]{%
    % #1    Name der Teilnehmer:in
    % #2    Schuljahr
    % #3    Was GROß
    % #4    Was genau
    % #5    Klassenstufe/Zusatz
    % #6    Inhalte
    % #7    Ort
    % #8    Datum
    % #9    Wessen Unterschrift
    \begin{picture}(0,0)
        \put(0.5,-4.25){%
            \includegraphics[scale=0.5]{mpv_green_vector}
        }
        \put(12.5,-5){%
            \includegraphics[scale=0.1]{gregor_graduiert}
        }
    \end{picture}

    \begin{center}
    \large\bfseries
        \vspace{5cm}
        \scalebox{6.0}{\textsc{URKUNDE}}
        \\[25mm]
        \textbf{\Huge #1}
        \\[10mm]
        hat #2 erfolgreich am
        \\[10mm]
        \textbf{\Huge #3}
        \\[10mm]
        \ifthenelse{\equal{#5}{}}{%
            des
        }{%
            der #4 #5 des
        }
        \\[10mm]
        \textbf{\Huge Mathezirkels Augsburg}
        \\[10mm]
        teilgenommen.
    \end{center}

    \vfill

    \ifthenelse{\equal{\inhalte}{}}{}{
    \begin{tabular}{lp{15cm}}
        \textbf{Inhalte:} & #6
    \end{tabular}
    }

    \vfill

    \normalsize
    #7, den #8 \hfill \rule{8cm}{1px} \\
    \hspace*{\fill} #9

    \clearpage
}


\newcommand{\campurkunde}[8]{%
    % #1    Name der Teilnehmer:in
    % #2    Schuljahr
    % #3    Was GROß
    % #4    Inhalte
    % #5    Ort
    % #6    Datum
    % #7    Wessen Unterschrift
    % #8    Bezeichnung für die Themen
    \begin{picture}(0,0)
        \put(0.5,-4.25){%
            \includegraphics[scale=0.5]{mpv_green_vector}
        }
        \put(12.5,-5){%
            \includegraphics[scale=0.1]{gregor_graduiert}
        }
    \end{picture}

    \begin{center}
    \large\bfseries
        \vspace{5cm}
        \scalebox{6.0}{\textsc{URKUNDE}}
        \\[25mm]
        \textbf{\Huge #1}
        \\[10mm]
        hat #2 erfolgreich am
        \\[10mm]
        \textbf{\Huge #3}
        \\[10mm]
        des
        \\[10mm]
        \textbf{\Huge Mathezirkels Augsburg}
        \\[10mm]
        teilgenommen.
    \end{center}

    \vfill

    \ifthenelse{\equal{\inhalte}{}}{}{
        \textbf{#8}

        \vspace{-3mm}

        \begin{center}
            \begin{minipage}{0.9\textwidth}
                #4
            \end{minipage}
        \end{center}
    }

    \vfill

    \normalsize
    #5, den #6 \hfill \rule{8cm}{1px} \\
    \hspace*{\fill} #7\\
    \hspace*{\fill} für das Team vom Mathecamp

    \clearpage
}
