%% zirkel.cls
%% Copyright 2023 Mathezirkel Augsburg
%
% This work may be distributed and/or modified under the
% conditions of the LaTeX Project Public License, either version 1.3
% of this license or (at your option) any later version.
% The latest version of this license is in
%   http://www.latex-project.org/lppl.txt
% and version 1.3 or later is part of all distributions of LaTeX
% version 2005/12/01 or later.
%
% This work has the LPPL maintenance status `maintained'.
%
% The Current Maintainer of this work is Felix Stärk.
%
% This work consists of all files listed in manifest.txt.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%                     														  %%
%% Klasse für Korrespondenzzirkelbriefe des Mathezirkel                       %%
%%                                                                            %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{zirkel}[2022/10/18 LaTeX class]

\LoadClass[a4paper,ngerman]{scrartcl}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Pakete                                                                     %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Sprache und Schrift
\RequirePackage[ngerman]{babel}
\RequirePackage[utf8]{inputenc}
\RequirePackage[T1]{fontenc}
\RequirePackage{lmodern}

% Mathematische Pakete
\RequirePackage{amsmath,amsthm,amssymb,amscd}
\RequirePackage{mathtools, stmaryrd}

% Graphiken
\RequirePackage{xcolor}
\RequirePackage{graphicx}

% Umgebungen
\RequirePackage{environ}
\RequirePackage{framed}

% Hyperlinks
\RequirePackage{url}
\RequirePackage{hyperref}

% Tabellen
\RequirePackage{enumitem}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Einstellbare Variablen                                                     %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\newcommand{\schuljahr}{}
\newcommand{\klasse}{}
\newcommand{\titel}{}
\newcommand{\untertitel}{}
\newcommand{\name}{}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Header                                                                     %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\setlength{\unitlength}{1cm}

\newlength{\titleskip}
\setlength{\titleskip}{1.3em}
\newcommand{\makeheader}{%
    \thispagestyle{empty}%
    \begin{picture}(0,0)
        \put(0,0){%
            \vbox{%
                \noindent 
                \begin{tabular}{ll}
                    Mathezirkel \hspace*{2cm} & Schuljahr \schuljahr \\
                    Universität Augsburg & Klassenstufe \klasse \\
                    \name
                \end{tabular}
            }
        }
        \put(13,-1.8){\includegraphics[scale=0.10]{cover}}
    \end{picture}%
    \vspace*{1.7cm}
    \begin{center}
        \Large \textbf{%
            \textsf{%
                \titel \\
                \normalsize \untertitel
            }
        }
    \end{center}
    \vspace{\titleskip}
}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Umgebungen                                                                 %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\definecolor{shadecolor}{rgb}{.93,.93,.93}

\theoremstyle{definition}
\newtheorem{definition}{Definition}
\newtheorem{aufg}{Aufgabe}
\newenvironment{aufgabe}[1][]
    {\begin{shaded}\vspace{-0.3cm}\begin{aufg}\emph{#1} \par\medskip}
    {\end{aufg}\vspace{-0.3cm}\end{shaded}}
\newtheorem*{loesung}{L\"osung}
\newtheorem*{hinweis}{Hinweis}
\newtheorem{beispiel}{Beispiel}
\theoremstyle{plain}
\newtheorem{theorem}{Satz}
\newenvironment{satz}
    {\begin{shaded}\vspace{-0.3cm}\begin{theorem}}
    {\end{theorem}\vspace{-0.3cm}\end{shaded}}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Layout                                                                     %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Seitenränder
\RequirePackage[left=2.5cm, right=2.5cm, top=3.5cm, bottom=4cm]{geometry}

% Einzug der ersten Zeile
\setlength{\parindent}{0cm}

% Satz
\clubpenalty=10000
\widowpenalty=10000
\displaywidowpenalty=10000


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Mathematische Symbole                                                      %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\newcommand{\N}{\mathbb{N}}
\newcommand{\Z}{\mathbb{Z}}
\newcommand{\Q}{\mathbb{Q}}
\newcommand{\R}{\mathbb{R}}
\newcommand{\C}{\mathbb{C}}
