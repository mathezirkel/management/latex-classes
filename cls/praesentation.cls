%% presentation.cls
%% Copyright 2023 Mathezirkel Augsburg
%
% This work may be distributed and/or modified under the
% conditions of the LaTeX Project Public License, either version 1.3
% of this license or (at your option) any later version.
% The latest version of this license is in
%   http://www.latex-project.org/lppl.txt
% and version 1.3 or later is part of all distributions of LaTeX
% version 2005/12/01 or later.
%
% This work has the LPPL maintenance status `maintained'.
%
% The Current Maintainer of this work is Felix Stärk.
%
% This work consists of all files listed in manifest.txt.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%                     														  %%
%% Class for presentations of the Mathezirkel                                 %%
%%                                                                            %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{praesentation}[2023/02/05 LaTeX class]
\LoadClass[xcolor=table]{beamer}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Packages                                                                   %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Language and fonts
\RequirePackage[ngerman]{babel}
\RequirePackage[utf8]{inputenc}
\RequirePackage[T1]{fontenc}
\RequirePackage{lmodern}

% Mathematics
\RequirePackage{amsmath, amsthm, amssymb, amsfonts, mathtools, stmaryrd}

% Graphics and color
\RequirePackage{xcolor}
\definecolor{mathezirkel}{RGB}{50,32,139}
\RequirePackage{graphicx}

% Links
\RequirePackage{qrcode}
\RequirePackage{hyperref}
\RequirePackage{url}

% Enumeration
\RequirePackage{enumitem}

% Tabulars
\RequirePackage{tabularx}
\RequirePackage{longtable}
\RequirePackage{colortbl}
\RequirePackage{array}

% Datetime
\RequirePackage[showdow=false,calc]{datetime2}
% \DTMdisplaystyle prints the date without year
% \DTMDisplaystyle prints the date containing the year
\DTMnewdatestyle{defaultdateformat}{%
    \renewcommand{\DTMdisplaydate}[4]{%
        \ifDTMshowdow\DTMweekdayname{##4},~den~\fi
        \DTMtwodigits{##3}.~\DTMmonthname{##2}%
    }
    \renewcommand{\DTMDisplaydate}[4]{%
        \ifDTMshowdow\DTMweekdayname{##4},~den~\fi
        \DTMtwodigits{##3}.~\DTMmonthname{##2}~\number##1%
    }
}
\DTMsetdatestyle{defaultdateformat}
\DTMnewtimestyle{defaulttimeformat}{%
    \renewcommand{\DTMdisplaytime}[3]{%
        \DTMtwodigits{##1}.\DTMtwodigits{##2}%
    }
}
\DTMsettimestyle{defaulttimeformat}

% Programming
\RequirePackage{xstring}
\RequirePackage{xifthen}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Layout                                                                     %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\setlength{\parskip}{\medskipamount}
\setlength{\parindent}{0pt}

\clubpenalty=10000
\widowpenalty=10000
\displaywidowpenalty=10000

\setlength{\unitlength}{1cm}

\usetheme{Madrid}
\usecolortheme[named=mathezirkel]{structure}
\usefonttheme[onlymath]{serif}
\setbeamertemplate{navigation symbols}{}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Methods & Commands                                                         %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%
% \urlQR{<link>}
%
% Places a QR code besides the argument containing its text.
%
\newcommand{\urlQR}[1]{%
    \begin{center}
        \begin{minipage}[t]{0.7\linewidth}
            \url{#1}
        \end{minipage}
        \begin{minipage}[t]{0.2\linewidth}
            \qrcode[height=1.7cm]{#1}
        \end{minipage}
    \end{center}
}


\newcommand{\urkundenframe}[4]{%
    % #1=ZirkelID,
    % #2=Zirkelart,
    % #3=Klassenstufe(n),
    % #4=Zirkelleiter:innen
    \def\zirkel{#1}
    \begin{frame}[t]{Urkundenverleihung}
        \begin{small}
            \begin{center}
                \textbf{
                    \Large #2\\[0.2cm]
                    \IfSubStr{#3}{und}{Klassen}{Klasse} #3
                }
                #4
                \vspace{1em}

                \begin{longtable}{rl}
                    \DTLforeach*[\DTLiseq{\zirkelID}{#1}]{abschlussliste}{%
                        \zirkelID=ZirkelID,
                        \vorname=Vorname,
                        \nachname=Nachname,
                        \klassen=Klassenstufen,
                        \preis=PreisID%
                    }{%
                        \DTLiffirstrow{}{\\}
                        \DTLifstringeq{\preis}{}{}{\\\kill\rowcolor{mathezirkel!25}}
                        \vorname & \nachname
                    }
                \end{longtable}
            \end{center}
        \end{small}
    \end{frame}
}

\newcommand{\leiternamen}[4]{%
    \ifthenelse{\equal{#4}{}}{%
        \ifthenelse{\equal{#3}{}}{%
            \ifthenelse{\equal{#2}{}}{%
                #1
            }{%
                #1~und~#2
            }
        }{%
            #1,~#2~und~#3
        }
    }{%
        #1,~#2,~#3~und~#4
    }
}
