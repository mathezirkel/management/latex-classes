# LaTeX class files

Das Repository enthält LaTeX Klassen für die Organisation des Mathezirkels Augsburg.

## Installation

```commandline
$ mkdir -p $(kpsewhich -var-value TEXMFHOME)/mathezirkel/
$ ln -s ./*.cls $(kpsewhich -var-value TEXMFHOME)/mathezirkel/
```

## License

The files in [manifest.txt](./manifest.txt) are licensed under [LPPL-1-3c](https://www.latex-project.org/lppl/lppl-1-3c.txt), see [LICENSE](.LICENSE).
